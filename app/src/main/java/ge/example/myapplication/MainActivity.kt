package ge.example.myapplication

import android.app.GameManager
import android.graphics.Color
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.Toast
import java.util.ArrayList

class MainActivity : AppCompatActivity(), View.OnClickListener {
    private lateinit var Button1: Button
    private lateinit var Button2: Button
    private lateinit var Button3: Button
    private lateinit var Button4: Button
    private lateinit var Button5: Button
    private lateinit var Button6: Button
    private lateinit var Button7: Button
    private lateinit var Button8: Button
    private lateinit var Button9: Button
    private lateinit var reset: Button


    private var firstPlayer = ArrayList<Int>()
    private var secondPlayer = ArrayList<Int>()

    private var activePlayer = 1

    private fun init() {
        Button1 = findViewById(R.id.button_1)
        Button2 = findViewById(R.id.button_2)
        Button3 = findViewById(R.id.button_3)
        Button4 = findViewById(R.id.button_4)
        Button5 = findViewById(R.id.button_5)
        Button6 = findViewById(R.id.button_6)
        Button7 = findViewById(R.id.button_7)
        Button8 = findViewById(R.id.button_8)
        Button9 = findViewById(R.id.button_9)
        reset = findViewById(R.id.reset)

        Button1.setOnClickListener(this)
        Button2.setOnClickListener(this)
        Button3.setOnClickListener(this)
        Button4.setOnClickListener(this)
        Button5.setOnClickListener(this)
        Button6.setOnClickListener(this)
        Button7.setOnClickListener(this)
        Button8.setOnClickListener(this)
        Button9.setOnClickListener(this)
        reset.setOnClickListener(this)
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()


    }

    override fun onClick(clickedView: View?) {
        if (clickedView is Button) {
            var ButtonNumber = 0
            when (clickedView.id) {
                R.id.button_1 -> ButtonNumber = 1
                R.id.button_2 -> ButtonNumber = 2
                R.id.button_3 -> ButtonNumber = 3
                R.id.button_4 -> ButtonNumber = 4
                R.id.button_5 -> ButtonNumber = 5
                R.id.button_6 -> ButtonNumber = 6
                R.id.button_7 -> ButtonNumber = 7
                R.id.button_8 -> ButtonNumber = 8
                R.id.button_9 -> ButtonNumber = 9

            }
            if (ButtonNumber != 0) {
                playGame(clickedView, ButtonNumber)
            }
        }

    }

    private fun playGame(clickedView: Button, ButtonNumber: Int) {
        if (activePlayer == 1) {
            clickedView.text = "x"
            clickedView.setBackgroundColor(Color.YELLOW)
            activePlayer = 2
            firstPlayer.add(ButtonNumber)
        } else {
            clickedView.text = "0"
            clickedView.setBackgroundColor(Color.RED)
            activePlayer = 1
            secondPlayer.add(ButtonNumber)
        }
        clickedView.isEnabled = false
        check()

    }

    private fun check() {
        var winnerPlayer = 0
        if (firstPlayer.contains(1) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(1) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(4) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(4) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(7) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(7) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(2) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(2) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(5) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(5) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }
        if (firstPlayer.contains(8) && firstPlayer.contains(2) && firstPlayer.contains(3)) {
            winnerPlayer = 1
        }
        if (secondPlayer.contains(8) && secondPlayer.contains(2) && secondPlayer.contains(3)) {
            winnerPlayer = 2
        }

        if (winnerPlayer == 1) {
            Toast.makeText(this, "First!!!", Toast.LENGTH_SHORT).show()

        }
        if (winnerPlayer == 2) {
            Toast.makeText(this, "Second!!!", Toast.LENGTH_SHORT).show()
        } else {
            Toast.makeText(this, "ups", Toast.LENGTH_SHORT).show()
        }
    }
}